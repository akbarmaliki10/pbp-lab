<h1>Lab Answer</h1>


<h2>Apakah perbedaan antara JSON dan XML?</h2>
Perbedaannnya yaitu JSON didasarkan dari JavaScript sedangkan XML diturunkan dari SGML. Perbedaan
selanjutnya adalah JSON merepresentasikan sebuah Objects sedangkan XML menggunakan tag seperti HTML
untuk merepresentasikan data. Perbedaan selanjutnya adalah file JSON lebih mudah untuk dibaca dibandingkan 
dengan XML yang sulit untuk dibaca. Perbedaan yang lain adalah JSON tidak menggunakan tag sedangkan
XML memakai start tag dan end tag.
<h2>Apakah perbedaan antara HTML dan XML?</h2>
Perbedaannya yaitu HTML bertujuan untuk menampilkan data sedangkan XML berfungsi untuk menyimpan
data. Perbedaan lainnya yaitu HTML tidak case sensitive sedangkan XML sebaliknya. Pada HTML semua
tidak semua element wajib memakai closing tag sedangkan pada XML diwajibkan untuk memakai closing
tag. Perbedaan lainnya yaitu pada HTML kita bisa menghindari error kecil sehingga tampilan tidak akan
terpengaruh sedangkan XML tidak mentoleransi adanya error.
