import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lab_6',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(title: 'Vaksinfo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  // var _controller = TextEditingController();

  final List<String> imageList = [
    'assets/images/carousel0.jpeg',
    'assets/images/carousel1.jpeg',
    'assets/images/carousel2.jpeg'
  ];

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/images/logo2.png",
                height: 50,
                width: 100,
              ),
              Image.asset(
                "assets/images/logo.png",
                height: 50,
                width: 100,
              ),
            ],
          ),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          actions: [
            PopupMenuButton(
              itemBuilder: (context) => [
                PopupMenuItem(
                    child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "ARTIKEL",
                    style: TextStyle(
                        color: Colors.white, fontFamily: 'Montserrat'),
                  ),
                )),
                PopupMenuItem(
                    child: Text(
                  "INFO VAKSIN",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
                )),
                PopupMenuItem(
                    child: Text(
                  "LOKASI VAKSIN",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
                )),
                PopupMenuItem(
                    child: Text(
                  "STATISTIK",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
                )),
                PopupMenuItem(
                    child: Text(
                  "TANYA JAWAB",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
                )),
                PopupMenuItem(
                    child: Text(
                  "LOGIN",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'Montserrat'),
                )),
              ],
              color: Colors.pink,
            ),
          ],
        ),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: CarouselSlider(
                options: CarouselOptions(
                    autoPlay: true,
                    enlargeCenterPage: true,
                    enableInfiniteScroll: false),
                items: imageList
                    .map((e) => ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Image.asset(
                                e,
                                width: 1050,
                                height: 350,
                                fit: BoxFit.cover,
                              )
                            ],
                          ),
                        ))
                    .toList(),
              ),
            ),
            Container(
                margin: EdgeInsets.all(15),
                // padding: EdgeInsets.all(10),
                // width: 250,
                // height: 50,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.pink),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {},
                  child: Text('Lihat lebih lengkap disini'),
                  // child: Text(
                  //   'Lihat lebih lengkap disini',
                  //   style: TextStyle(
                  //       color: Colors.white, fontFamily: 'Montserrat'),
                  // ),
                )),
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Container(
                decoration: BoxDecoration(color: Colors.redAccent, boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(4, 8),
                  )
                ]),
                padding: EdgeInsets.fromLTRB(0, 30, 0, 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Image.asset(
                          "assets/images/body1.png",
                          height: 50,
                          width: 100,
                        ),
                        Text("Statistik")
                      ],
                    ),
                    Column(
                      children: [
                        Image.asset(
                          "assets/images/body2.png",
                          height: 50,
                          width: 100,
                        ),
                        Text("Info Vaksin")
                      ],
                    ),
                    Column(
                      children: [
                        Image.asset(
                          "assets/images/body3.png",
                          height: 50,
                          width: 100,
                        ),
                        Text("Lokasi Vaksin")
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // Container(
            //     margin: EdgeInsets.all(10),
            //     child: Text(
            //       "Form Feedback",
            //       style:
            //           TextStyle(color: Colors.black, fontFamily: 'Montserrat'),
            //     )),
            // Container(
            //   margin: EdgeInsets.all(10),
            //   decoration: BoxDecoration(
            //       color: Colors.white,
            //       border: Border.all(color: Colors.black),
            //       borderRadius: BorderRadius.all(Radius.circular(10))),
            //   child: TextFormField(
            //     // The validator receives the text that the user has entered.
            //     controller: _controller,
            //     validator: (value) {
            //       if (value == null || value.isEmpty) {
            //         return 'Please enter some text';
            //       }
            //       return null;
            //     },
            //   ),
            // ),
            // TextButton(
            //   style: ButtonStyle(
            //       foregroundColor:
            //           MaterialStateProperty.all<Color>(Colors.white),
            //       backgroundColor:
            //           MaterialStateProperty.all<Color>(Colors.pink)),
            //   onPressed: _controller.clear,
            //   child: Text('Submit'),
            // )
          ],
        ),
      ),
      bottomNavigationBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: BottomAppBar(
          child: Image.asset(
            "assets/images/logo.png",
            height: 30,
            width: 100,
          ),
          color: Colors.pink,
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
