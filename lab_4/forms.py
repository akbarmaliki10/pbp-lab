from django.forms import ModelForm
from lab_2.models import Note

class NoteForm(ModelForm) :
    class Meta :
        model = Note
        fields = ['to','froM', 'title', 'message']

    error_messages = {
		'required' : 'Please Type'
	}


