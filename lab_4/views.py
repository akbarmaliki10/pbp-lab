from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect


# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note':note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    if request.method == "POST" :
        form = NoteForm(request.POST)
        if form.is_valid() :
            form.save()
            return HttpResponseRedirect('/lab-4/note-list')
    else :
        form = NoteForm()

    return render(request, "lab4_form.html", {'form' : form})

def note_list(request) :
    note = Note.objects.all()
    response = {'note':note}
    return render(request, 'lab4_note_list.html', response)

