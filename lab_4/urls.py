from django.urls import path

from .views import add_note, note_list, index

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add-note',add_note),
    path('note-list',note_list),
]
