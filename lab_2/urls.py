from django.urls import path
from .views import index
from .views import xml
from .views import json

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('xml', xml),
    path('json',json),
]
