from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

# Create your views here.

@login_required(login_url="/admin/login/")
def index (request) :
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url="/admin/login/")
def add_friend (request) :
    if request.method == "POST" :
        form = FriendForm(request.POST)
        if form.is_valid() :
            form.save()
            return HttpResponseRedirect('/lab-3')
    else :
        form = FriendForm()

    return render(request, "form.html", {'form' : form})
